## Usage

```
check-payslip -c /path/to/config.conf
```

Example config file provided in `config/example.conf`.

## Building

Install [bazel](https://www.bazel.build/) and run:

```
bazel build //cmd/check-payslip
```

## Licensing

This project is under the MIT license.
