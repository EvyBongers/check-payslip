package main

import (
	"fmt"
	"io"
	"log"
	"net/url"

	"github.com/studio-b12/gowebdav"
	httpntlm "github.com/vadimi/go-http-ntlm"
)

type Fetcher struct {
	davHost     url.URL
	davDomain   string
	davUserName string
	davPassword string
	davClient   *gowebdav.Client
}

func NewFetcher(host, domain, username, password string) Fetcher {
	f := Fetcher{
		davHost: url.URL{
			Scheme: "https",
			Host:   host,
		},
		davDomain: domain,
		davUserName: username,
		davPassword: password,
	}

	f.davClient = gowebdav.NewClient(f.davHost.String(), f.davUserName, f.davPassword)
	f.davClient.SetTransport(
		&httpntlm.NtlmTransport{
			Domain:   f.davDomain,
			User:     f.davUserName,
			Password: f.davPassword,
		},
	)

	return f
}

func (f Fetcher) Fetch(fileName string) (io.ReadCloser, error) {
	documentUri := fmt.Sprintf("/personal/%s/Personal Documents/%s", f.davUserName, fileName)
	log.Printf("Attempting to fetch document: %s\n", documentUri)

	return f.davClient.ReadStream(documentUri)
}
