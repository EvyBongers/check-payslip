package main

import (
	"fmt"
	"log"
	"net/mail"
	"net/smtp"

	"github.com/pkg/errors"
	"github.com/scorredoira/email"
	cp "gitlab.com/EvyBongers/check-payslip/pkg/proto/config"
	"gitlab.com/EvyBongers/check-payslip/pkg/smtp_auth_login"
)

type Sender struct {
	smtpAddr string
	smtpAuth smtp.Auth
}

func NewSender(smtpHost string, smtpPort uint32, credentials *cp.SMTPCredentials) Sender {
	var auth smtp.Auth
	if credentials != nil {
		auth = smtp_auth_login.LoginAuth(credentials.Username, credentials.Password)
	}
	return Sender{
		smtpAddr: fmt.Sprintf("%s:%d", smtpHost, smtpPort),
		smtpAuth: auth,
	}
}

func (s Sender) Send(messageSubject, messageBody, mailFrom string, rcptList, fileList []string) error {
	message := email.NewMessage(messageSubject, messageBody)

	fromAddress, err := mail.ParseAddress(mailFrom)
	if err != nil {
		return errors.Wrap(err, "Failed to parse sender address: "+mailFrom)
	}
	message.From = *fromAddress

	for _, rcptTo := range rcptList {
		rcptAddress, err := mail.ParseAddress(rcptTo)
		if err != nil {
			return errors.Wrap(err, "Failed to parse recipient address: "+rcptTo)
		}
		message.AddTo(*rcptAddress)
	}

	for _, filePath := range fileList {
		if err := message.Attach(filePath); err != nil {
			log.Printf("Failed to attach %s: %v\n", filePath, err)
		}
	}

	err = email.Send(s.smtpAddr, s.smtpAuth, message)
	if err != nil {
		err = errors.Wrap(err, "Failed to send email")
	}
	return err
}
