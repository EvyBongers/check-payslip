package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/golang/protobuf/proto"
	cp "gitlab.com/EvyBongers/check-payslip/pkg/proto/config"
)

type File struct {
	Name     string
	Contents []byte
}

type MailInfo struct {
	MessageSubject string
	MessageBody    string
	FileName       string
}

func main() {
	var refDate time.Time

	configurationFile := flag.String("c", "", "Configuration file")
	refDateString := flag.String("d", "", "Date string (YYYY-MM-DD)")
	flag.Parse()
	if *configurationFile == "" {
		log.Fatal("No configuration file specified")
	}

	data, err := ioutil.ReadFile(*configurationFile)
	if err != nil {
		log.Fatal(err)
	}
	var config cp.Configuration
	if err := proto.UnmarshalText(string(data), &config); err != nil {
		log.Fatal(err)
	}

	fetcher := NewFetcher(config.Webdav.Server, config.Webdav.Credentials.Domain, config.Webdav.Credentials.Username, config.Webdav.Credentials.Password)
	sender := NewSender(config.Smtp.Server, config.Smtp.Port, config.Smtp.Credentials)
	if *refDateString == "" {
		refDate = time.Now()
	}else{
		refDate, err = time.Parse("2006-01-02", *refDateString)
	}
	thisMonth := refDate.Format("012006")
	lastYear := refDate.AddDate(-1, 0, 0).Format("2006")
	dateString := refDate.Format("January 2006")

	mailInfos := [...]MailInfo{
		MailInfo{
			MessageSubject: fmt.Sprintf("Pay slip %s", dateString),
			MessageBody:    fmt.Sprintf("Please find attached your pay slip for %s.\n", dateString),
			FileName:       fmt.Sprintf("Salarisstrook_%s_%08d_%s.pdf", thisMonth, config.EmployeeNumber, strings.ToUpper(config.Webdav.Credentials.Username)),
		},
		MailInfo{
			MessageSubject: fmt.Sprintf("Year end calculation %s", lastYear),
			MessageBody:    fmt.Sprintf("Please find attached your year end calculation for %s.\n", lastYear),
			FileName:       fmt.Sprintf("Eindejaarsberekening_%s_%08d_%s.pdf", thisMonth, config.EmployeeNumber, strings.ToUpper(config.Webdav.Credentials.Username)),
		},
		MailInfo{
			MessageSubject: fmt.Sprintf("Annual statement %s", lastYear),
			MessageBody:    fmt.Sprintf("Please find attached your annual statement for %s.\n", lastYear),
			FileName:       fmt.Sprintf("Jaaropgaaf_%s_%08d_%s.pdf", lastYear, config.EmployeeNumber, strings.ToUpper(config.Webdav.Credentials.Username)),
		},
	}

	for _, mailInfo := range mailInfos {
		filePath := filepath.Join(config.CopyDir, mailInfo.FileName)
		if err := download(&fetcher, config.CopyDir, mailInfo.FileName); err != nil {
			if !os.IsExist(err) {
				log.Printf("Failed to download: %v", err)
				os.Remove(filePath)
			} else {
				log.Printf("Local copy of %s already exists", mailInfo.FileName)
			}
			continue
		}

		log.Printf("Sending %s", mailInfo.FileName)
		if err := sender.Send(mailInfo.MessageSubject, mailInfo.MessageBody, config.MailFrom, config.RcptTo, []string{filePath}); err != nil {
			log.Printf("Sending message failed: %v", err)
		}
	}
}

func download(fetcher *Fetcher, targetDir, fileName string) error {
	filePath := filepath.Join(targetDir, fileName)
	f, err := os.OpenFile(filePath, os.O_CREATE|os.O_WRONLY|os.O_EXCL, 0666)
	if err != nil {
		return err
	}
	defer f.Close()

	fileStream, err := fetcher.Fetch(fileName)
	if err != nil {
		return err
	}
	defer fileStream.Close()

	log.Printf("Writing file: %v", filePath)
	_, err = io.Copy(f, fileStream)
	return err
}
